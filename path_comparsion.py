#-*- coding:utf-8 -*-
import copy
    
def select_algo( *arg ):
    id = arg[0]
    name = arg[1]
    value = Mtx[name]
    start_point = border = int( len(arg[2])-1 )
    cnt = 0
    coordinate = ( id, border )    
    num = value[border]
    flag = 0

    while True:
        print ( 'In ( %d, %d )--> %d'%( id, border , num ) )
        ( id, border, num ) = comparison( id, num, border )
        cnt += 1
        print ( 'Step %d, computed: %d ( %d, %d )'%( cnt, num, id, border ) )
        
        if ( id, border ) == ( 0, 0 ) or cnt == sum( coordinate ):
            print ( 'Final --> %d'%( num ) )
            MtxN[name][start_point] = num
            
            if flag: break
            elif start_point: start_point -= 1
            else: start_point = 0
            
            id = arg[0]
            border = start_point
            coordinate = ( id, border )
            cnt = 0
            num = value[border]
        
            if coordinate == ( id, 0 ):
                flag = 1
    
def comparison( id, num, border ):
    try:
        if Mtx[Matrix_parm[str(int(id-1))]][border] >= Mtx[Matrix_parm[str(id)]][border] and id > 0:
            id -= 1
            num += Mtx[Matrix_parm[str(id)]][border]
            print ( '^', Mtx[Matrix_parm[str(id)]][border] )
        elif Mtx[Matrix_parm[str(id)]][border-1] > Mtx[Matrix_parm[str(id)]][border] and border > 0:
            border -= 1
            num += Mtx[Matrix_parm[str(id)]][border]
            print ( '<', Mtx[Matrix_parm[str(id)]][border] )
        else:
            id -= 1
            num += Mtx[Matrix_parm[str(id)]][border]
            print ( '^', Mtx[Matrix_parm[str(id)]][border] )
    except:
        if border:
            border -= 1
            num += Mtx[Matrix_parm[str(id)]][border]
            print ( '<', Mtx[Matrix_parm[str(id)]][border] )

    return ( id, border, num )

def show( colum):
    print ( '\n\n------ origin ------' )
    for elm in colum:
        print ( Mtx[elm] )
    print ( '------ computed ------' )
    for elm in colum:
        print ( MtxN[elm] )
    print ( '\n' )

def main( parm ):
    Mtx_keys = []
    keys = sorted( parm.keys() )
    for key in keys:
        Mtx_keys.append( Matrix_parm[key])
    for idx, elm in enumerate( Mtx_keys ):
        select_algo( idx, elm, Mtx[elm] )
    show( Mtx_keys )
    

if __name__ == '__main__':
    
    Matrix_parm = { '0': 'M1', 
                    '1': 'M2', 
                    '2': 'M3' }
    Mtx = { 'M1': [ 5, 7, 6, 2, 8, 3, 9 ],
            'M2': [ 2, 1, 3, 6, 3, 8, 1 ],
            'M3': [ 9, 3, 1, 5, 2, 7, 3 ] }
    MtxN = copy.deepcopy(Mtx)    
    
    main( Matrix_parm )